package com.example.mask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageButton = findViewById(R.id.button);
        imageButton.setBackgroundResource(R.drawable._1);
        imageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button:
                int i = (int) (1+ Math.random()*28);
                switch (i){
                    case 1:
                        imageButton.setBackgroundResource(R.drawable._1);
                        break;
                    case 2:
                        imageButton.setBackgroundResource(R.drawable._2);
                        break;
                    case 3:
                        imageButton.setBackgroundResource(R.drawable._3);
                        break;
                    case 4:
                        imageButton.setBackgroundResource(R.drawable._4);
                        break;
                    case 5:
                        imageButton.setBackgroundResource(R.drawable._5);
                        break;
                    case 6:
                        imageButton.setBackgroundResource(R.drawable._6);
                        break;
                    case 7:
                        imageButton.setBackgroundResource(R.drawable._7);
                        break;
                    case 8:
                        imageButton.setBackgroundResource(R.drawable._8);
                        break;
                    case 9:
                        imageButton.setBackgroundResource(R.drawable._9);
                        break;
                    case 10:
                        imageButton.setBackgroundResource(R.drawable._10);
                        break;
                    case 11:
                        imageButton.setBackgroundResource(R.drawable._11);
                        break;
                    case 12:
                        imageButton.setBackgroundResource(R.drawable._12);
                        break;
                    case 13:
                        imageButton.setBackgroundResource(R.drawable._13);
                        break;
                    case 14:
                        imageButton.setBackgroundResource(R.drawable._14);
                        break;
                    case 15:
                        imageButton.setBackgroundResource(R.drawable._15);
                        break;
                    case 16:
                        imageButton.setBackgroundResource(R.drawable._16);
                        break;
                    case 17:
                        imageButton.setBackgroundResource(R.drawable._17);
                        break;
                    case 18:
                        imageButton.setBackgroundResource(R.drawable._18);
                        break;
                    case 19:
                        imageButton.setBackgroundResource(R.drawable._19);
                        break;
                    case 20:
                        imageButton.setBackgroundResource(R.drawable._20);
                        break;
                    case 21:
                        imageButton.setBackgroundResource(R.drawable._21);
                        break;
                    case 22:
                        imageButton.setBackgroundResource(R.drawable._22);
                        break;
                    case 23:
                        imageButton.setBackgroundResource(R.drawable._23);
                        break;
                    case 24:
                        imageButton.setBackgroundResource(R.drawable._24);
                        break;
                    case 25:
                        imageButton.setBackgroundResource(R.drawable._25);
                        break;
                    case 26:
                        imageButton.setBackgroundResource(R.drawable._26);
                        break;
                    case 27:
                        imageButton.setBackgroundResource(R.drawable._27);
                        break;
                    case 28:
                        imageButton.setBackgroundResource(R.drawable._28);
                        break;
                }
                break;
        }
    }
}
